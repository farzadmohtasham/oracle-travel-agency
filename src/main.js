import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import providers from './provides/index.provide';
import antDesignVue from '@/plugins/ant-design-vue';
import persianDatetimePicker from '@/plugins/persian-datetime-picker';
import '@/includes/tailwindcss';
import '@/includes/global-styles';
import './assets/tailwind.css';

const app = createApp(App);

app.use(store);
app.use(router);
app.use(antDesignVue);
app.use(persianDatetimePicker);
app.use(providers);
app.mount('#app');
