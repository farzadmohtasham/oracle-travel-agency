export default {
  beforeMount() {
    this.toggleFooter();
  },
  unmounted() {
    this.toggleFooter();
  },
};
