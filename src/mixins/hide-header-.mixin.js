export default {
  beforeMount() {
    this.toggleHeader();
  },
  unmounted() {
    this.toggleHeader();
  },
};
