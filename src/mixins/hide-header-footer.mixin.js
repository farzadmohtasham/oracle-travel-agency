export default {
  beforeMount() {
    this.toggleHeader();
    this.toggleFooter();
  },
  unmounted() {
    this.toggleHeader();
    this.toggleFooter();
  },
};
