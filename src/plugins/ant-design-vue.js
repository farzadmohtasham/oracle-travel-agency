import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';

export default {
  install(app) {
    app.use(Antd);
  },
};
