import Vue3PersianDatetimePicker from 'vue3-persian-datetime-picker';

export default {
  install(app) {
    app.component('DatePicker', Vue3PersianDatetimePicker);
  },
};
