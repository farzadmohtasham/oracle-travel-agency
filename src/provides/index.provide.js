import ScoreLevels from './score-levels';

export default {
  install(app) {
    app.provide('ScoreLevels', ScoreLevels);
  },
};
