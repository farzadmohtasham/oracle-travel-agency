const levels = {
  0: {
    title: 'پایه',
    min: 0,
    max: 200,
    cardImgUrl: '/images/white-wave-background.jpg',
  },
  1: {
    title: 'برنز',
    min: 201,
    max: 400,
    cardImgUrl: '/images/circle-shape-colored-background.jpg',
  },
  2: {
    title: 'نقره',
    min: 401,
    max: 600,
    cardImgUrl: '/images/blue-yellow-purple-background-shapes-and-waves.jpg',
  },
  3: {
    title: 'طلا ۱۸ عیار',
    min: 601,
    max: 800,
    cardImgUrl: '/images/blue-and-purple-waves-background.jpg',
  },
  4: {
    title: 'طلا ۲۱ عیار',
    min: 801,
    max: 1000,
    cardImgUrl: '/images/awesome-yellow-and-purple-shapes-background.jpg',
  },
  5: {
    title: 'فول',
    min: 1001,
    max: 9999,
    cardImgUrl: '/images/awesome-yellow-and-purple-shapes-background.jpg',
  },
};

export default class ScoreLevels {
  static getLevels() {
    return levels;
  }

  static findUserLevel(score) {
    let foundLevel = null;
    let levelKey = null;
    Object.entries(levels)
      .forEach(([key, val]) => {
        if (score >= val.min && score <= val.max) {
          foundLevel = val;
          levelKey = key;
        }
      });
    return {
      foundLevel,
      levelKey,
    };
  }

  static nextLevel(score) {
    const { levelKey } = this.findUserLevel(score);
    const currentLevel = levels[levelKey];
    const nextLevel = levels[Number(levelKey) + 1];
    return {
      neededScore: currentLevel.max - score,
      nextLevelTitle: nextLevel.title,
      nextLevelData: nextLevel,
    };
  }

  static levelIsFull(score) {
    const { levelKey } = this.findUserLevel(score);
    if (Object.entries(levelKey).length === Number(levelKey)) {
      return true;
    }
    return false;
  }
}
