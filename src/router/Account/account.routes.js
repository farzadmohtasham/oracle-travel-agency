import Profile from '@/views/Account/Profile/Profile.vue';
import store from '@/store/index';
import Ticket from '@/views/Account/Profile/Tickets/Ticket.vue';

function beforeEnterMiddleware(to, from, next) {
  if (store.getters.loggedIn === false) {
    next('/auth');
  } else {
    next();
  }
}

export default [
  {
    name: 'Account-Profile',
    path: '/account/profile/:tab?',
    component: Profile,
    beforeEnter: [beforeEnterMiddleware],
  },
  {
    name: 'Account-Profile-Tickets-Ticket',
    path: '/account/profile/tickets/:id',
    component: Ticket,
    beforeEnter: [beforeEnterMiddleware],
  },
];
