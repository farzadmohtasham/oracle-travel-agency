import Auth from '@/views/Auth/Auth.vue';
import store from '@/store/index';

export default [
  {
    path: '/auth/',
    name: 'Auth',
    component: Auth,
    beforeEnter: (to, from, next) => {
      if (store.getters.loggedIn) {
        next({ name: 'Account-Profile' });
      } else {
        next();
      }
    },
  },
];
