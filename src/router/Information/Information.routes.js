import ContactUs from '@/views/Information/Contact-Us.vue';
import AboutUs from '@/views/Information/About-Us.vue';

export default [
  {
    path: '/contact-us',
    name: 'ContactUs',
    component: ContactUs,
  },
  {
    path: '/about-us',
    name: 'AboutUs',
    component: AboutUs,
  },
];
