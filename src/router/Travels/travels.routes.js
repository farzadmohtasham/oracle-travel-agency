import Travels from '@/views/Travels/Travels.vue';
import TravelRegister from '@/views/Travels/Travel-Register.vue';

export default [
  {
    name: 'Travels',
    path: '/travels/',
    component: Travels,
  },
  {
    name: 'Travel-Register',
    path: '/travels/register',
    component: TravelRegister,
  },
];
