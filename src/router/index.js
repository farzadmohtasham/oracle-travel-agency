import { createRouter, createWebHistory } from 'vue-router';
import Home from '../views/Home.vue';
import AuthRoutes from '@/router/Auth/Auth.routes';
import InformationRoutes from '@/router/Information/Information.routes';
import AccountRoutes from '@/router/Account/account.routes';
import TravelsRoutes from '@/router/Travels/travels.routes';

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  ...AuthRoutes,
  ...InformationRoutes,
  ...AccountRoutes,
  ...TravelsRoutes,
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
