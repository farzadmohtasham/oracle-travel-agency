export default {
  brand: '',
  price: {
    adult: '',
    young: '',
    child: '',
  },
  labels: [],
  from: '',
  to: '',
  date: '',
  time: {
    fly: {
      h: '',
      m: '',
    },
    landing: {
      h: '',
      m: '',
    },
  },
  otherInfo: {
    travelNumber: '',
    rateClass: '',
    allowedLoadAmount: '',
    terminalNumber: '',
    remainingSeats: '',
    aircraft: '',
  },
};
