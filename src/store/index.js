import { createStore } from 'vuex';
import AppDataModule from '@/store/modules/data/data.store';
import AuthModule from '@/store/modules/auth/auth.store';
import UserModule from '@/store/modules/user/user.store';
import UIModule from '@/store/modules/ui/ui.store';

export default createStore({
  modules: {
    AppDataModule,
    AuthModule,
    UserModule,
    UIModule,
  },
});
