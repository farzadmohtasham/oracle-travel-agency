export default {
  state: {
    loggedIn: false,
    user: null,
  },
  getters: {
    loggedIn: (state) => state.loggedIn,
    user: (state) => state.user,
    tickets: (state) => state.user.tickets,
  },
  mutations: {
    login(state, user) {
      state.loggedIn = true;
      state.user = user;
    },
    logout(state) {
      state.loggedIn = false;
      state.user = null;
    },
    updateUser(state, user) {
      state.user = user;
    },
  },
  actions: {
    logout({ commit }, { $route, $router }) {
      commit('logout');
      if ($route.name !== 'Home') {
        $router.push({ name: 'Home' });
      }
    },
    updateUser({ commit }, user) {
      commit('updateUser', user);
    },
  },
};
