export default {
  state: {
    showHeader: true,
    showFooter: true,
    modalOpen: false,
  },
  getters: {
    showHeader: (state) => state.showHeader,
    showFooter: (state) => state.showFooter,
    modalOpen: (state) => state.modalOpen,
  },
  mutations: {
    changeHeaderFooter(state, {
      prop,
      val,
    }) {
      state[prop] = val;
    },
    toggleModal(state) {
      state.modalOpen = !state.modalOpen;
    },
  },
  actions: {
    toggleHeader(ctx) {
      ctx.commit('changeHeaderFooter', {
        prop: 'showHeader',
        val: !ctx.getters.showHeader,
      });
    },
    toggleFooter(ctx) {
      ctx.commit('changeHeaderFooter', {
        prop: 'showFooter',
        val: !ctx.getters.showFooter,
      });
    },
  },
};
