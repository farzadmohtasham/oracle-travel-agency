export default {
  state: {
    users: [
      {
        username: {
          firstName: 'فرزاد',
          lastName: 'محتشم',
        },
        email: null,
        phoneNumber: '9307685472',
        password: '12345678',
        score: 526,
        credit: 2878000,
        sex: 'male',
        dateOfBirth: '1379/7/21',
        homePhoneNumber: '04137409987',
        nationalCode: '1540555920',
        bankInfo: {
          cardNumber: '5022291085690434',
          accountNumber: '9874324',
          shabaNumber: '2345482979233849',
        },
        transactions: [
          {
            code: '29478',
            type: 'increase-charge',
            amount: 349000,
            title: 'افزایش اعتبار حساب',
            status: 'success',
          },
          {
            code: '34509',
            type: 'increase-charge',
            amount: 657000,
            title: 'افزایش اعتبار حساب',
            status: 'unsuccessful',
          },
          {
            code: '90247',
            type: 'decrease-charge',
            amount: 599000,
            title: 'خرید بلیط هواپیما تهران-تبریز',
            status: 'awaiting-payment',
          },
        ],
        tickets: [
          {
            id: 0,
            status: 'open',
            category: 'out-travel',
            type: 'send-a-stamped-ticket',
            title: 'درخواست تغییر بلیط پرواز',
            orderNumber: '309875',
            replies: [
              {
                id: 0,
                from: 'user',
                msg: 'سلام، ممنون میشم بلیط من رو تغییر بدین، باتشکر از آژانس اوراکل',
              },
              {
                id: 1,
                from: 'admin',
                msg: 'سلام، چشم کمی صبر کنین',
              },
              {
                id: 2,
                from: 'admin',
                msg: 'خب تغییر دادم امر دیگه ای ندارین؟',
              },
              {
                id: 3,
                from: 'user',
                msg: 'نه ممنون، واقعا لطف کردین، شب بخیر',
              },
            ],
          },
        ],
      },
      {
        username: {
          firstName: 'زهرا',
          lastName: 'رضاپور',
        },
        email: 'fake_email_2@gmail.com',
        phoneNumber: '9037685485',
        password: '12345678',
        score: 349,
        credit: 0,
        sex: 'male',
        dateOfBirth: '1379/7/21',
        homePhone: '04137409987',
        nationalCode: '1540555920',
        bankInfo: {
          cardNumber: '5022291085690434',
          accountNumber: '9874324',
          shabaNumber: '2345482979233849',
        },
        transactions: [
          {
            code: '29478',
            type: 'increase-charge',
            amount: 349000,
            title: 'افزایش اعتبار حساب',
            status: 'success',
          },
          {
            code: '34509',
            type: 'increase-charge',
            amount: 657000,
            title: 'افزایش اعتبار حساب',
            status: 'success',
          },
          {
            code: '90247',
            type: 'decrease-charge',
            amount: 599000,
            title: 'خرید بلیط هواپیما تهران-تبریز',
            status: 'success',
          },
        ],
      },
      {
        username: {
          firstName: 'مهسا',
          lastName: 'محمدی',
        },
        email: 'fake_email_3@gmail.com',
        phoneNumber: '9147726131',
        password: '12345678',
        score: 0,
        credit: 0,
        sex: 'male',
        dateOfBirth: '1379/7/21',
        homePhone: '04137409987',
        nationalCode: '1540555920',
        bankInfo: {
          cardNumber: '5022291085690434',
          accountNumber: '9874324',
          shabaNumber: '2345482979233849',
        },
      },
      {
        username: {
          firstName: 'علی',
          lastName: 'دلیر',
        },
        email: 'fake_email_4@gmail.com',
        phoneNumber: '9141210683',
        password: '12345678',
        score: 0,
        credit: 0,
        sex: 'male',
        dateOfBirth: '1379/7/21',
        homePhone: '04137409987',
        nationalCode: '1540555920',
        bankInfo: {
          cardNumber: '5022291085690434',
          accountNumber: '9874324',
          shabaNumber: '2345482979233849',
        },
      },
    ],
  },
  getters: {
    users: (state) => state.users,
  },
  mutations: {
    addUser(state, newUser) {
      state.users.push(newUser);
    },
  },
  actions: {
    findUserByPhoneNumber({ getters }, phoneNumber) {
      return getters.users.filter((user) => user.phoneNumber === phoneNumber)[0] || null;
    },
    addUser({ commit }, newUser) {
      commit('addUser', newUser);
    },
  },
};
