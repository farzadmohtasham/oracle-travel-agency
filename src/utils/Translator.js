const words = [
  {
    en: 'caspian',
    fa: 'کاسپین',
  },
  {
    en: 'tehran',
    fa: 'تهران',
  },
  {
    en: 'ahvaz',
    fa: 'اهواز',
  },
  {
    en: 'systemic',
    fa: 'سیستمی',
  },
  {
    en: 'economic',
    fa: 'اکونومی',
  },
  {
    en: 'geshm-air',
    fa: 'قشم ایر',
  },
  {
    en: 'karvan',
    fa: 'کاروان',
  },
  {
    en: 'aseman',
    fa: 'آسمان',
  },
  {
    en: 'ata',
    fa: 'آتا',
  },
  {
    en: 'shiraz',
    fa: 'شیراز',
  },
  {
    en: 'kish',
    fa: 'کیش',
  },
  {
    en: 'tabriz',
    fa: 'تبریز',
  },
  {
    en: 'esfahan',
    fa: 'اصفحان',
  },
  {
    en: 'bandar-abbas',
    fa: 'بندر عباس',
  },
  {
    en: 'mashhad',
    fa: 'مشهد',
  },
  {
    en: 'increase-charge',
    fa: 'افزایش اعتبار',
  },
  {
    en: 'decrease-charge',
    fa: 'کاهش اعتبار(جهت خرید خدمات)',
  },
  {
    en: 'success',
    fa: 'موفق',
  },
  {
    en: 'unsuccessful',
    fa: 'ناموفق',
  },
  {
    en: 'awaiting-payment',
    fa: 'در انتظار پرداخت',
  },
  {
    en: 'all',
    fa: 'همه',
  },
  {
    en: 'open',
    fa: 'فعال',
  },
  {
    en: 'send-a-stamped-ticket',
    fa: 'ارسال بلیط مهر شده',
  },
  {
    en: 'national-code-change',
    fa: 'اصلاح کد ملی',
  },
  {
    en: 'wheelchair-request',
    fa: 'درخواست ویلچر',
  },
  {
    en: 'travel-class-change',
    fa: 'تغییر کلاس پرواز',
  },
  {
    en: 'male',
    fa: 'آقا',
  },
  {
    en: 'female',
    fa: 'خانم',
  },
];

export default class Translator {
  static translate(enWord) {
    const foundWords = words.filter((word) => word.en === enWord);
    if (foundWords.length >= 1) return foundWords[0].fa;
    return 'چنین کلمه ای در هنگام ترجمه یافت نشد';
  }
}
