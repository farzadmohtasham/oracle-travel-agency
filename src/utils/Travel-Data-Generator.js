import { random } from 'lodash';
import innerTravelsSchema from '@/schema/travels-schema/inner-travels.schema';
import {
  travelPrices,
  innerTravelBrands,
  travelDates,
  travelAirCrafts,
  travelClasses,
  travelLabels,
  innerTravelCities,
} from '@/utils/data/travels/inner-travels-data';

function flyTimeRandGen() {
  const schema = {
    fly: {
      h: random(1, 22),
      m: random(0, 59),
    },
    landing: {
      h: '',
      m: '',
    },
  };

  schema.landing.h = schema.fly.h + 1;
  schema.landing.m = random(0, 59);

  return schema;
}
let travelCounter = 0;

export default class TravelDataGenerator {
  static innerTravels(numberOfGen) {
    const generatedTravels = [];

    innerTravelCities.forEach((firstCity, index) => {
      innerTravelCities.forEach((secondCity, secIndex) => {
        if (secIndex === index) return;

        Array.from(Array(numberOfGen).keys()).forEach(async () => {
          const newTravelRec = Object.create(innerTravelsSchema);

          newTravelRec.id = travelCounter;
          travelCounter += 1;

          newTravelRec.brand = innerTravelBrands[random(0, innerTravelBrands.length - 1)];

          Object.keys(newTravelRec.price).forEach((key) => {
            newTravelRec.price[key] = travelPrices[random(0, travelPrices.length - 1)];
          });

          travelLabels.forEach((label) => {
            const pushLabel = random(0, 1);
            if (pushLabel === 1) newTravelRec.labels.push(label);
          });

          newTravelRec.from = firstCity;
          newTravelRec.to = secondCity;

          newTravelRec.date = travelDates[random(0, travelDates.length - 1)];

          newTravelRec.time = flyTimeRandGen();

          newTravelRec.otherInfo = {
            travelNumber: random(0, 9999),
            rateClass: travelClasses[random(0, travelClasses.length - 1)],
            allowedLoadAmount: random(15, 30),
            terminalNumber: random(0, 7),
            remainingSeats: random(0, 50),
            aircraft: travelAirCrafts[random(0, travelAirCrafts.length - 1)],
          };

          generatedTravels.push(newTravelRec);
        });
      });
    });

    return generatedTravels;
  }
}
