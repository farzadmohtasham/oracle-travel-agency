export default (h, m) => {
  const hour = h.toString().length === 1 ? `0${h}` : h;
  const minute = m.toString().length === 1 ? `0${m}` : m;
  return `${hour}:${minute}`;
};
