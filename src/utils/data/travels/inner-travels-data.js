export const innerTravelCities = [
  {
    short: 'th',
    full: 'tehran',
  },
  {
    short: 'ahv',
    full: 'ahvaz',
  },
  {
    short: 'shi',
    full: 'shiraz',
  },
  {
    short: 'mash',
    full: 'mashhad',
  },
  {
    short: 'ban',
    full: 'bandar-abbas',
  },
  {
    short: 'esf',
    full: 'esfahan',
  },
  {
    short: 'tab',
    full: 'tabriz',
  },
  {
    short: 'ki',
    full: 'kish',
  },
];

export const innerTravelBrands = [
  {
    name: 'caspian',
    img: 'https://cdn.alibaba.ir/static/img/airlines/Domestic/IV.png',
  },
  {
    name: 'ata',
    img: 'https://cdn.alibaba.ir/static/img/airlines/Domestic/I3.png',
  },
  {
    name: 'aseman',
    img: 'https://cdn.alibaba.ir/static/img/airlines/Domestic/EP.png',
  },
  {
    name: 'geshm-air',
    img: 'https://cdn.alibaba.ir/static/img/airlines/Domestic/QB.png',
  },
  {
    name: 'karvan',
    img: 'https://cdn.alibaba.ir/static/img/airlines/Domestic/NV.png',
  },
];

export const travelPrices = [
  300000,
  450000,
  386000,
  760000,
  167000,
  320000,
  890000,
  728000,
  829000,
  220000,
  819000,
  122000,
  348000,
];

export const travelLabels = ['systemic', 'economic'];

export const travelDates = [
  '1400/11/15',
  '1400/11/16',
  '1400/11/17',
  '1400/11/18',
  '1400/11/19',
  '1400/11/20',
  '1400/11/21',
  '1400/11/22',
  '1400/11/23',
  '1400/11/24',
  '1400/11/25',
  '1400/11/26',
  '1400/11/27',
  '1400/11/28',
  '1400/11/29',
  '1400/11/30',
  '1400/12/01',
  '1400/12/02',
  '1400/12/03',
  '1400/12/04',
  '1400/12/05',
  '1400/12/06',
  '1400/12/07',
  '1400/12/08',
  '1400/12/09',
  '1400/12/10',
  '1400/12/11',
  '1400/12/12',
  '1400/12/13',
  '1400/12/14',
  '1400/12/15',
  '1400/12/16',
  '1400/12/17',
  '1400/12/18',
  '1400/12/19',
  '1400/12/20',
  '1400/12/21',
  '1400/12/22',
  '1400/12/23',
  '1400/12/24',
  '1400/12/25',
  '1400/12/26',
  '1400/12/27',
  '1400/12/28',
  '1400/12/29',
  '1400/12/30',
];

export const travelClasses = 'ABCQWPRTMZX'.split('');

export const travelAirCrafts = [
  'Boeing 747-8',
  'Boeing 777',
  'Boeing C-17 Globemaster',
  'Boeing 787-10',
  'Boeing 767-400ER',
];
